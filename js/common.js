var $corp_live_slide_top, $corp_live_slide_but;

'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}

if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

$(function() {
    // placeholder
    //-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

    $('.partners__item').BlackAndWhite({
        hoverEffect: true, // default true
        // set the path to BnWWorker.js for a superfast implementation
        webworkerPath: false,
        // to invert the hover effect
        invertHoverEffect: false,
        // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)
        intensity: 1,
        speed: { //this property could also be just speed: value for both fadeIn and fadeOut
            fadeIn: 200, // 200ms for fadeIn animations
            fadeOut: 800 // 800ms for fadeOut animations
        },
        onImageReady: function(img) {
            // this callback gets executed anytime an image is converted
        }
    });

    //Slick slider - http://kenwheeler.github.io/slick/
    $('.js-slider').slick({
        arrows: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        fade: true,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
            }
        }]
    });

    $('.partners__line').slick({
        arrows: true,
        dots: false,
        slidesToShow: 5,
        responsive: [{
            breakpoint: 999,
            settings: {
                slidesToShow: 3,
                arrows: false,
            }
        }, {
            breakpoint: 544,
            settings: {
                slidesToShow: 1,
            }
        }, ]
    });

    $('.history__slider').slick({
        arrows: true,
        dots: false,
        slidesToShow: 3,
        responsive: [{
            breakpoint: 999,
            settings: {
                slidesToShow: 3,
            }
        }, {
            breakpoint: 544,
            settings: {
                slidesToShow: 1,
            }
        }, ]
    });

    //Match Height Plugin - https://github.com/liabru/jquery-match-height
    $('.js-match-height').matchHeight({
        byRow: true,
        property: 'height',
    });

    var overlay = $('.overlay'),
        popUp = $('.pop-up'),
        duration = 400,
        mobMenu = $('.b_header__menu > ul'),
        mobMenuBtn = $('.js-show-menu'),
        certSliderInit = false;

    $('.js-pop-up').on('click', function(e) {
        e.preventDefault();



        var popUpTarget = $(this).attr('data-pop');

        $('body').addClass('ov-h');
        $('.' + popUpTarget).css({
            'height': '100vh',
        })

        $('.' + popUpTarget).mCustomScrollbar();

        if (popUpTarget === "pop-up-certificates" && !certSliderInit) {
            //Единоразовая инициализация слайдера
            certSliderInit = !certSliderInit;
            certificateSlider();
        }
        if (popUpTarget === "pop-up-certificates") {
            //При открытия слайдера - скролим к слайду который был кликнут
            var targetSlider = $(this).attr('data-cert');
            certCurrentSlider(targetSlider);
        }
        overlay.fadeIn(duration, function() {
            $('.' + popUpTarget).show().css({
                    'top': $(window).scrollTop()
                })
                .addClass('active');
        });
    });

    function closePop() {
        popUp.removeClass('active');
        overlay.fadeOut(duration, function() {
            popUp.css({
                'top': 0
            })
        });
        setTimeout(function() {
            popUp.hide();
        }, 400);
        $('body').removeClass('ov-h');
    };

    overlay.on('click', function() {
        closePop();
        if ($(window).outerWidth() < 991) {
            mobMenu.slideUp();
            mobMenuBtn.removeClass('active');
        }
    });

    $('.js-close-pop').on('click', function(e) {
        e.preventDefault();
        closePop();
    });

    $('.has-dropdown > a').on('click', function(e) {
        if ($(window).outerWidth() < 550) {
            $('.logo').fadeOut();
        }
        if ($(window).outerWidth() < 1099) {
            e.preventDefault();

            // if ($(this).hasClass('active')) {
            //     $(this).siblings('.b_header__dropdown').slideUp();
            //     $(this).removeClass('active');
            //
            // } else {

            $('.js-dropdown-back').fadeIn();
            $(this).siblings('.b_header__dropdown').addClass('visible');
            // $(this).addClass('active');
            // }
        }
    });

    $('.js-dropdown-back').on('click', function(e) {
        e.preventDefault();
        $('.b_header__dropdown.visible').removeClass('visible');

        $('.logo').fadeIn();
        $('.js-dropdown-back').fadeOut();
    })

    mobMenuBtn.on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            overlay.fadeOut(duration);
            $(this).removeClass('active');
            mobMenu.slideUp();

            $('.logo').fadeIn();
            $('.js-dropdown-back').fadeOut();
            $('.b_header__dropdown.visible').removeClass('visible');
        } else {
            overlay.fadeIn(duration);
            $(this).addClass('active');
            mobMenu.slideDown();
        }
    })

    $('.js-animation').on('inview', function(event, isInView) {
        if (isInView) {
            $('.radial-progress').each(function() {
                var radius = $(this).attr('data-progress');
                $(this).find('.mask.full,.fill').css({
                    'transform': 'rotateZ(' + 180 / 100 * radius + 'deg)',
                })
                $(this).find('.fill.fix').css({
                    'transform': 'rotateZ(' + 180 / 100 * radius * 2 + 'deg)',
                })
            });
        } else {
            $('.radial-progress').each(function() {
                var radius = $(this).attr('data-progress');
                $(this).find('.mask.full,.fill').css({
                    'transform': 'rotateZ(0deg)',
                })
                $(this).find('.fill.fix').css({
                    'transform': 'rotateZ(0deg)',
                })
            });
        }
    });

    $('.strategy__slider').slick({
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
    });

    $corp_live_slide_top = $('.corp-live__slide-wrap_top').slick({
        dots: false,
        infinite: true,
        arrows: false,
        swipeToSlide: true,
        speed: 1000,
        slidesToShow: 1,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 3000,
        variableWidth: true
    });
    $corp_live_slide_but = $('.corp-live__slide-wrap_but').slick({
        dots: false,
        infinite: true,
        swipeToSlide: true,
        arrows: false,
        speed: 1000,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        centerMode: true,
        variableWidth: true
    });
    // $corp_live_slide_top.on('afterChange', function (event, slick, currentSlide, nextSlide) {
    //     $corp_live_slide_but.slick('slickNext');

    // })

    $('.corp-live__slide-wrap').on('mousewheel', function(e) {
        e.preventDefault();
        if (e.deltaY == 1) {
            $corp_live_slide_but.slick('slickNext');
            $corp_live_slide_top.slick('slickNext');
        } else {
            $corp_live_slide_but.slick('slickPrev');
            $corp_live_slide_top.slick('slickPrev');
        }
    })

    $('.slider-dft').slick({
        arrows: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: false,
            }
        }]
    });

    var mainCertSlider = $('.certificates-slider');
    var navCertSlider = $('.certificate-info-slider');

    function certificateSlider() {
        mainCertSlider.slick({
            arrows: true,
            dots: true,
            fade: true,
            asNavFor: '.certificate-info-slider'
        });

        navCertSlider.slick({
            arrows: false,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.certificates-slider',
            fade: true,
            cssEase: 'linear'
        });

        setTimeout(function() {
            $('.certificates-slider').slick("refresh");
            $('.certificate-info-slider').slick("refresh");
        }, 1000);
    }

    //https://github.com/igorescobar/jQuery-Mask-Plugin
    $('.js-phone-mask').mask("+7(999)999-99-99");

    //Form Style - http://dimox.name/jquery-form-styler/
    $('.js-select-styler').styler();

    //Srcoll dropdown select - http://manos.malihu.gr/jquery-custom-content-scroller/
    $('.jq-selectbox ul').mCustomScrollbar({
        scrollInertia: 100
    });

    //Form Validate - https://jqueryvalidation.org/
    $('.js-form').each(function() {



        $(this).validate({
            rules: {
                firstName: {
                    required: true,
                },
                lastName: {
                    required: true,
                },
                phone: {
                    required: true,
                    minlength: 16,
                },
                email: {
                    required: true,
                    email: true,
                },
                text: {
                    required: true
                }
            },
            messages: {
                firstName: {
                    required: 'Введите имя',
                },
                lastName: {
                    required: 'Введите фамилию',
                },
                phone: {
                    required: 'Введите свой номер телефона',
                    minlength: 'Проверьте правильность введеного номера',
                },
                email: {
                    required: 'Введите свой email',
                    email: 'Проверьте правильность введеного email',
                },
                text: {
                    required: 'Введите свое сообщение'
                }
            }

        })

    })

    function certCurrentSlider(targetSlider) {
        console.log(typeof targetSlider);
        setTimeout(function() {
            mainCertSlider.slick('slickGoTo', targetSlider);
        }, duration);
    };

    var firstlat = 59.960621;
    var firstlng = 30.282655;

    google.maps.event.addDomListener(window, "load", initMap);

    function initMap() {
        var directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: "#ec008c",

            }
        });
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions({
            suppressMarkers: true
        });
        directionsDisplay.setOptions({
            preserveViewport: true
        });
        var directionsService = new google.maps.DirectionsService;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            scrollwheel: false,
            preserveViewport: true,
            center: {
                lat: firstlat,
                lng: firstlng
            },
            streetViewControl: false,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER

            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            styles: [{
                "featureType": "all",
                "elementType": "all",
                "stylers": [{
                    "saturation": -100
                }, {
                    "gamma": 0.5
                }]
            }]
        });
        var image = 'images/map-logo.png';
        var marker = new google.maps.Marker({
            position: {
                lat: 59.960621,
                lng: 30.282655
            },
            map: map,
            icon: image
        });
        directionsDisplay.setMap(map);

    }

    $(window).load(function() {
        $('.page-wrapper').css({
            'opacity': 1,
            'transition': '.3s ease',
        });
    });
});
